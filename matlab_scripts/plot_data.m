
clear; close all; clc;

%addpath('./utils');

dir_e300 = '~/rfnoc/e300/usr/lib/uhd/examples';
% uncomment to use data files produced beforehand
% dir_e300 = '.';

% plot for the condition 1 
% ./my_rfnoc_rx_to_file --duration 1 --freq 89.1e6 --rate 1e6 --gain 40 --format sc16 --block-id 0/gain_0 --block-args "gain=1.0" --args "type=e3x0,fpga=/home/root/newinstall/images/last/PR/e300.bit" --file rx_sc16_40db_diggain_1_static1_PR2_PR1.dat --fpga-path-pr1 /home/root/newinstall/images/last/PR/e310_core0_inst_gain_noc_gain2_partial.bit  --fpga-path-pr2 /home/root/newinstall/images/last/PR/e310_core0_inst_gain_noc_gain1_partial.bit 

file1 = fullfile(dir_e300, 'rx_sc16_40db_diggain_1_static1_PR2_PR1.dat');
file2 = fullfile(dir_e300, 'rx_sc16_40db_diggain_1_static1_PR2_PR1_PR.dat');
file3 = fullfile(dir_e300, 'rx_sc16_40db_diggain_1_static1_PR2_PR1_PR_PR.dat');

rx_sc16_40db_diggain_1_static1_PR2_PR1 = read_complex_short(file1);
rx_sc16_40db_diggain_1_static1_PR2_PR1_PR = read_complex_short(file2);
rx_sc16_40db_diggain_1_static1_PR2_PR1_PR_PR = read_complex_short(file3);

subplot(312), plot(rx_sc16_40db_diggain_1_static1_PR2_PR1_PR);grid;axis equal;
title('static1+partial2');
a = axis;

subplot(313), plot(rx_sc16_40db_diggain_1_static1_PR2_PR1_PR_PR);grid;axis equal;
title('static1+partial2+partial1');
axis(a);

subplot(311), plot(rx_sc16_40db_diggain_1_static1_PR2_PR1); grid; axis equal;
title('static1');
axis(a);

% plot for the condition 2 
% ./my_rfnoc_rx_to_file --duration 1 --freq 89.1e6 --rate 1e6 --gain 40 --format sc16 --block-id 0/gain2_0 --block-args "gain=1.0" --args "type=e3x0,fpga=/home/root/newinstall/images/last/PR/e300_gain2.bit" --file rx_sc16_40db_diggain_1_static2_PR1_PR2.dat --fpga-path-pr1 /home/root/newinstall/images/last/PR/e310_core0_inst_gain_noc_gain1_partial.bit  --fpga-path-pr2 /home/root/newinstall/images/last/PR/e310_core0_inst_gain_noc_gain2_partial.bit 

file1 = fullfile(dir_e300, 'rx_sc16_40db_diggain_1_static2_PR1_PR2.dat');
file2 = fullfile(dir_e300, 'rx_sc16_40db_diggain_1_static2_PR1_PR2_PR.dat');
file3 = fullfile(dir_e300, 'rx_sc16_40db_diggain_1_static2_PR1_PR2_PR_PR.dat');

rx_sc16_40db_diggain_1_static2_PR1_PR2 = read_complex_short(file1);
rx_sc16_40db_diggain_1_static2_PR1_PR2_PR = read_complex_short(file2);
rx_sc16_40db_diggain_1_static2_PR1_PR2_PR_PR = read_complex_short(file3);

figure;
subplot(311), plot(rx_sc16_40db_diggain_1_static2_PR1_PR2);grid;axis equal;
title('static2');
a = axis;

subplot(312), plot(rx_sc16_40db_diggain_1_static2_PR1_PR2_PR);grid;axis equal;
title('static2+partial1');
axis(a);

subplot(313), plot(rx_sc16_40db_diggain_1_static2_PR1_PR2_PR_PR); grid; axis equal;
title('static2+partial1+partial2');
axis(a);

function [iq_data] = read_complex_short(filename)
%   READ_COMPLEX_FLOAT Read complex data in float format.
%   [iq_data] = read_complex_float(filename) read data in the file "filename" 
%   as float values (first real second immaginary) into a complex vector 

fid = fopen(filename);
data = fread(fid, [2,inf], 'short','ieee-le');
fclose(fid);

iq_data = complex(data(1,:),data(2,:));

end

