//
// Copyright 2014-2016 Ettus Research LLC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


#include <uhd/device3.hpp>
#include <uhd/exception.hpp>
#include <uhd/rfnoc/radio_ctrl.hpp>
#include <uhd/rfnoc/source_block_ctrl_base.hpp>
#include <uhd/types/sensors.hpp>
#include <uhd/types/tune_request.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/thread.hpp>
#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <chrono>
#include <complex>
#include <csignal>
#include <fstream>
#include <iostream>
#include <thread>

// modifications SGA

#include <boost/filesystem.hpp>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <uhd/image_loader.hpp>
#include <uhd/types/device_addr.hpp>

int load_partial_bitstream(std::string fpga_path);
int setup_and_stream(std::string file, std::string streamargs, 
    uhd::device3::sptr usrp, std::string block_id, uhd::rfnoc::block_id_t radio_ctrl_id, 
    size_t radio_chan, std::string block_args, size_t spp, std::string format,
    size_t total_num_samps, size_t spb, double rate, double total_time,
    bool bw_summary, bool stats, bool enable_size_map, bool continue_on_bad_packet);

namespace fs = boost::filesystem;

// end modifications SGA

namespace po = boost::program_options;

const int64_t UPDATE_INTERVAL = 1; // 1 second update interval for BW summary

static bool stop_signal_called = false;
void sig_int_handler(int)
{
    stop_signal_called = true;
}

template <typename samp_type>
void recv_to_file(uhd::rx_streamer::sptr rx_stream,
    const std::string& file,
    const size_t samps_per_buff,
    const double rx_rate,
    const unsigned long long num_requested_samples,
    double time_requested       = 0.0,
    bool bw_summary             = false,
    bool stats                  = false,
    bool enable_size_map        = false,
    bool continue_on_bad_packet = false)
{
    unsigned long long num_total_samps = 0;

    uhd::rx_metadata_t md;
    std::vector<samp_type> buff(samps_per_buff);
    std::ofstream outfile;
    if (not file.empty()) {
        outfile.open(file.c_str(), std::ofstream::binary);
    }
    bool overflow_message = true;

    // setup streaming
    uhd::stream_cmd_t stream_cmd((num_requested_samples == 0)
                                     ? uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS
                                     : uhd::stream_cmd_t::STREAM_MODE_NUM_SAMPS_AND_DONE);
    stream_cmd.num_samps  = size_t(num_requested_samples);
    stream_cmd.stream_now = true;
    stream_cmd.time_spec  = uhd::time_spec_t();
    std::cout << "Issuing stream cmd" << std::endl;
    rx_stream->issue_stream_cmd(stream_cmd);

    const auto start_time = std::chrono::steady_clock::now();
    const auto stop_time =
        start_time + std::chrono::milliseconds(int64_t(1000 * time_requested));
    // Track time and samps between updating the BW summary
    auto last_update                     = start_time;
    unsigned long long last_update_samps = 0;

    typedef std::map<size_t, size_t> SizeMap;
    SizeMap mapSizes;

    // Run this loop until either time expired (if a duration was given), until
    // the requested number of samples were collected (if such a number was
    // given), or until Ctrl-C was pressed.
    while (not stop_signal_called
           and (num_requested_samples != num_total_samps or num_requested_samples == 0)
           and (time_requested == 0.0 or std::chrono::steady_clock::now() <= stop_time)) {
        const auto now = std::chrono::steady_clock::now();

        size_t num_rx_samps =
            rx_stream->recv(&buff.front(), buff.size(), md, 3.0, enable_size_map);

        if (md.error_code == uhd::rx_metadata_t::ERROR_CODE_TIMEOUT) {
            std::cout << boost::format("Timeout while streaming") << std::endl;
            break;
        }
        if (md.error_code == uhd::rx_metadata_t::ERROR_CODE_OVERFLOW) {
            if (overflow_message) {
                overflow_message = false;
                std::cerr
                    << boost::format(
                           "Got an overflow indication. Please consider the following:\n"
                           "  Your write medium must sustain a rate of %fMB/s.\n"
                           "  Dropped samples will not be written to the file.\n"
                           "  Please modify this example for your purposes.\n"
                           "  This message will not appear again.\n")
                           % (rx_rate * sizeof(samp_type) / 1e6);
            }
            continue;
        }
        if (md.error_code != uhd::rx_metadata_t::ERROR_CODE_NONE) {
            std::string error = str(boost::format("Receiver error: %s") % md.strerror());
            if (continue_on_bad_packet) {
                std::cerr << error << std::endl;
                continue;
            } else
                throw std::runtime_error(error);
        }

        if (enable_size_map) {
            SizeMap::iterator it = mapSizes.find(num_rx_samps);
            if (it == mapSizes.end())
                mapSizes[num_rx_samps] = 0;
            mapSizes[num_rx_samps] += 1;
        }

        num_total_samps += num_rx_samps;

        if (outfile.is_open()) {
            outfile.write((const char*)&buff.front(), num_rx_samps * sizeof(samp_type));
        }

        if (bw_summary) {
            last_update_samps += num_rx_samps;
            const auto time_since_last_update = now - last_update;
            if (time_since_last_update > std::chrono::seconds(UPDATE_INTERVAL)) {
                const double time_since_last_update_s =
                    std::chrono::duration<double>(time_since_last_update).count();
                const double rate = double(last_update_samps) / time_since_last_update_s;
                std::cout << "\t" << (rate / 1e6) << " MSps" << std::endl;
                last_update_samps = 0;
                last_update       = now;
            }
        }
    }
    const auto actual_stop_time = std::chrono::steady_clock::now();

    stream_cmd.stream_mode = uhd::stream_cmd_t::STREAM_MODE_STOP_CONTINUOUS;
    std::cout << "Issuing stop stream cmd" << std::endl;
    rx_stream->issue_stream_cmd(stream_cmd);

    // Run recv until nothing is left
    int num_post_samps = 0;
    do {
        num_post_samps = rx_stream->recv(&buff.front(), buff.size(), md, 3.0);
    } while (num_post_samps and md.error_code == uhd::rx_metadata_t::ERROR_CODE_NONE);

    if (outfile.is_open())
        outfile.close();

    if (stats) {
        std::cout << std::endl;

        const double actual_duration_seconds =
            std::chrono::duration<float>(actual_stop_time - start_time).count();

        std::cout << boost::format("Received %d samples in %f seconds") % num_total_samps
                         % actual_duration_seconds
                  << std::endl;
        const double rate = (double)num_total_samps / actual_duration_seconds;
        std::cout << (rate / 1e6) << " MSps" << std::endl;

        if (enable_size_map) {
            std::cout << std::endl;
            std::cout << "Packet size map (bytes: count)" << std::endl;
            for (SizeMap::iterator it = mapSizes.begin(); it != mapSizes.end(); it++)
                std::cout << it->first << ":\t" << it->second << std::endl;
        }
    }
}

typedef boost::function<uhd::sensor_value_t(const std::string&)> get_sensor_fn_t;

bool check_locked_sensor(std::vector<std::string> sensor_names,
    const char* sensor_name,
    get_sensor_fn_t get_sensor_fn,
    double setup_time)
{
    if (std::find(sensor_names.begin(), sensor_names.end(), sensor_name)
        == sensor_names.end())
        return false;

    auto setup_timeout = std::chrono::steady_clock::now()
                         + std::chrono::milliseconds(int64_t(setup_time * 1000));
    bool lock_detected = false;

    std::cout << boost::format("Waiting for \"%s\": ") % sensor_name;
    std::cout.flush();

    while (true) {
        if (lock_detected and (std::chrono::steady_clock::now() > setup_timeout)) {
            std::cout << " locked." << std::endl;
            break;
        }
        if (get_sensor_fn(sensor_name).to_bool()) {
            std::cout << "+";
            std::cout.flush();
            lock_detected = true;
        } else {
            if (std::chrono::steady_clock::now() > setup_timeout) {
                std::cout << std::endl;
                throw std::runtime_error(
                    str(boost::format(
                            "timed out waiting for consecutive locks on sensor \"%s\"")
                        % sensor_name));
            }
            std::cout << "_";
            std::cout.flush();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    std::cout << std::endl;
    return true;
}

int UHD_SAFE_MAIN(int argc, char* argv[])
{
    uhd::set_thread_priority_safe();

    // variables to be set by po
    std::string args, file, format, ant, subdev, ref, wirefmt, streamargs, radio_args,
        block_id, block_args;
    size_t total_num_samps, spb, radio_id, radio_chan;
    double rate, freq, gain, bw, total_time, setup_time;

    // setup the program options
    po::options_description desc("Allowed options");
    // clang-format off
    desc.add_options()
        ("help", "help message")
        ("file", po::value<std::string>(&file)->default_value("usrp_samples.dat"), "name of the file to write binary samples to")
        ("format", po::value<std::string>(&format)->default_value("sc16"), "File sample format: sc16, fc32, or fc64")
        ("duration", po::value<double>(&total_time)->default_value(0), "total number of seconds to receive")
        ("nsamps", po::value<size_t>(&total_num_samps)->default_value(0), "total number of samples to receive")
        ("spb", po::value<size_t>(&spb)->default_value(10000), "samples per buffer")
        ("streamargs", po::value<std::string>(&streamargs)->default_value(""), "stream args")
        ("progress", "periodically display short-term bandwidth")
        ("stats", "show average bandwidth on exit")
        ("sizemap", "track packet size and display breakdown on exit")
        ("null", "run without writing to file")
        ("continue", "don't abort on a bad packet")

        ("args", po::value<std::string>(&args)->default_value(""), "USRP device address args")
        ("setup", po::value<double>(&setup_time)->default_value(1.0), "seconds of setup time")

        ("radio-id", po::value<size_t>(&radio_id)->default_value(0), "Radio ID to use (0 or 1).")
        ("radio-chan", po::value<size_t>(&radio_chan)->default_value(0), "Radio channel")
        ("radio-args", po::value<std::string>(&radio_args), "Radio channel")
        ("rate", po::value<double>(&rate)->default_value(1e6), "RX rate of the radio block")
        ("freq", po::value<double>(&freq)->default_value(0.0), "RF center frequency in Hz")
        ("gain", po::value<double>(&gain), "gain for the RF chain")
        ("ant", po::value<std::string>(&ant), "antenna selection")
        ("bw", po::value<double>(&bw), "analog frontend filter bandwidth in Hz")
        ("ref", po::value<std::string>(&ref), "reference source (internal, external, mimo)")
        ("skip-lo", "skip checking LO lock status")
        ("int-n", "tune USRP with integer-N tuning")

        ("block-id", po::value<std::string>(&block_id)->default_value(""), "If block ID is specified, this block is inserted between radio and host.")
        ("block-args", po::value<std::string>(&block_args)->default_value(""), "These args are passed straight to the block.")

        // modifications SGA
        ("fpga-path-pr1", po::value<std::string>()->default_value(""), "FPGA-PR1 path (uses default if none specified)")
        ("fpga-path-pr2", po::value<std::string>()->default_value(""), "FPGA-PR2 path (uses default if none specified)")
        // end modifications SGA

    ;
    // clang-format on
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    // print the help message
    if (vm.count("help")) {
        std::cout << boost::format("UHD/RFNoC RX samples to file %s") % desc << std::endl;
        std::cout << std::endl
                  << "This application streams data from a single channel of a USRP "
                     "device to a file.\n"
                  << std::endl;
        return ~0;
    }

    bool bw_summary = vm.count("progress") > 0;
    bool stats      = vm.count("stats") > 0;
    if (vm.count("null") > 0) {
        file = "";
    }
    bool enable_size_map        = vm.count("sizemap") > 0;
    bool continue_on_bad_packet = vm.count("continue") > 0;

    if (enable_size_map) {
        std::cout << "Packet size tracking enabled - will only recv one packet at a time!"
                  << std::endl;
    }

    if (format != "sc16" and format != "fc32" and format != "fc64") {
        std::cout << "Invalid sample format: " << format << std::endl;
        return EXIT_FAILURE;
    }

    // modifications SGA

    // get the path to the the partial bitstream 1
    std::string fpga_path_pr1;
    fpga_path_pr1     = vm["fpga-path-pr1"].as<std::string>();
    // Clean up fpga_path_pr, if given
    if (fpga_path_pr1 != "") {
#ifndef UHD_PLATFORM_WIN32
        if (fpga_path_pr1.find("~") == 0) {
            fpga_path_pr1.replace(0, 1, getenv("HOME"));
        }
#endif /* UHD_PLATFORM_WIN32 */
        fpga_path_pr1 = fs::absolute(fpga_path_pr1).string();
    }

    // get the path to the the partial bitstream 2
    std::string fpga_path_pr2;
    fpga_path_pr2     = vm["fpga-path-pr2"].as<std::string>();
    // Clean up fpga_path_pr, if given
    if (fpga_path_pr2 != "") {
#ifndef UHD_PLATFORM_WIN32
        if (fpga_path_pr2.find("~") == 0) {
            fpga_path_pr2.replace(0, 1, getenv("HOME"));
        }
#endif /* UHD_PLATFORM_WIN32 */
        fpga_path_pr2 = fs::absolute(fpga_path_pr2).string();
    }

    // end modifications SGA

    /************************************************************************
     * Create device and block controls
     ***********************************************************************/
    std::cout << std::endl;
    std::cout << boost::format("Creating the USRP device with: %s...") % args
              << std::endl;
    uhd::device3::sptr usrp = uhd::device3::make(args);
    // Create handle for radio object
    uhd::rfnoc::block_id_t radio_ctrl_id(0, "Radio", radio_id);
    // This next line will fail if the radio is not actually available
    uhd::rfnoc::radio_ctrl::sptr radio_ctrl =
        usrp->get_block_ctrl<uhd::rfnoc::radio_ctrl>(radio_ctrl_id);
    std::cout << "Using radio " << radio_id << ", channel " << radio_chan << std::endl;

    /************************************************************************
     * Set up radio
     ***********************************************************************/
    radio_ctrl->set_args(radio_args);
    if (vm.count("ref")) {
        std::cout << "TODO -- Need to implement API call to set clock source."
                  << std::endl;
        // Lock mboard clocks TODO
        // usrp->set_clock_source(ref);
    }

    // set the sample rate
    if (rate <= 0.0) {
        std::cerr << "Please specify a valid sample rate" << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << boost::format("Setting RX Rate: %f Msps...") % (rate / 1e6) << std::endl;
    radio_ctrl->set_rate(rate);
    std::cout << boost::format("Actual RX Rate: %f Msps...")
                     % (radio_ctrl->get_rate() / 1e6)
              << std::endl
              << std::endl;

    // set the center frequency
    if (vm.count("freq")) {
        std::cout << boost::format("Setting RX Freq: %f MHz...") % (freq / 1e6)
                  << std::endl;
        uhd::tune_request_t tune_request(freq);
        if (vm.count("int-n")) {
            // tune_request.args = uhd::device_addr_t("mode_n=integer"); TODO
        }
        radio_ctrl->set_rx_frequency(freq, radio_chan);
        std::cout << boost::format("Actual RX Freq: %f MHz...")
                         % (radio_ctrl->get_rx_frequency(radio_chan) / 1e6)
                  << std::endl
                  << std::endl;
    }

    // set the rf gain
    if (vm.count("gain")) {
        std::cout << boost::format("Setting RX Gain: %f dB...") % gain << std::endl;
        radio_ctrl->set_rx_gain(gain, radio_chan);
        std::cout << boost::format("Actual RX Gain: %f dB...")
                         % radio_ctrl->get_rx_gain(radio_chan)
                  << std::endl
                  << std::endl;
    }

    // set the IF filter bandwidth
    if (vm.count("bw")) {
        // std::cout << boost::format("Setting RX Bandwidth: %f MHz...") % (bw/1e6) <<
        // std::endl; radio_ctrl->set_rx_bandwidth(bw, radio_chan); // TODO std::cout <<
        // boost::format("Actual RX Bandwidth: %f MHz...") %
        // (radio_ctrl->get_rx_bandwidth(radio_chan)/1e6) << std::endl << std::endl;
    }

    // set the antenna
    if (vm.count("ant")) {
        radio_ctrl->set_rx_antenna(ant, radio_chan);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(int64_t(1000 * setup_time)));

    // check Ref and LO Lock detect
    if (not vm.count("skip-lo")) {
        // TODO
        // check_locked_sensor(usrp->get_rx_sensor_names(0), "lo_locked",
        // boost::bind(&uhd::usrp::multi_usrp::get_rx_sensor, usrp, _1, radio_id),
        // setup_time); if (ref == "external")
        // check_locked_sensor(usrp->get_mboard_sensor_names(0), "ref_locked",
        // boost::bind(&uhd::usrp::multi_usrp::get_mboard_sensor, usrp, _1, radio_id),
        // setup_time);
    }

    size_t spp = radio_ctrl->get_arg<int>("spp");

    // modifications SGA
    if(setup_and_stream(file, streamargs, usrp, block_id, radio_ctrl_id, radio_chan, block_args,
        spp, format, total_num_samps, spb, rate, total_time, bw_summary, stats, enable_size_map, 
        continue_on_bad_packet) == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }
    // end modifications SGA


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // modified by SGA

    // if there is a partial bitstream path, load it and stream again, appending "_PR" to the filename
    if (fpga_path_pr1 != "") {

        if (load_partial_bitstream(fpga_path_pr1) == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }

        // append "_PR" to the filename
        file.replace(file.end()-4,file.end(),"_PR.dat");
        std::cout << file << std::endl;

        if(setup_and_stream(file, streamargs, usrp, block_id, radio_ctrl_id, radio_chan, block_args,
            spp, format, total_num_samps, spb, rate, total_time, bw_summary, stats, enable_size_map, 
            continue_on_bad_packet) == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }
    }


    // if there is a partial bitstream path, load it and stream again, appending "_PR" to the filename
    if (fpga_path_pr2 != "") {

        if (load_partial_bitstream(fpga_path_pr2) == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }

        // append "_PR" to the filename
        file.replace(file.end()-4,file.end(),"_PR.dat");
        std::cout << file << std::endl;

        if(setup_and_stream(file, streamargs, usrp, block_id, radio_ctrl_id, radio_chan, block_args,
            spp, format, total_num_samps, spb, rate, total_time, bw_summary, stats, enable_size_map, 
            continue_on_bad_packet) == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }

    }

    // end modified by SGA

    return EXIT_SUCCESS;

}

// modifications SGA
int gpio_alloc(char *num, char *dir) {

    // example of equivalent allocation done in the command line with num = "54" dir = "out":
    // # echo 54 > /sys/class/gpio/export
	// # echo out > /sys/class/gpio/gpio54/direction

    int valuefd, exportfd, directionfd, l;
    char str[256];

    // The GPIO has to be exported to be able to see it in sysfs
    exportfd = open("/sys/class/gpio/export", O_WRONLY);

	if (exportfd < 0) {
        printf("Cannot open GPIO to export it\n");
        exit(1);
	}

	l = strlen(num);
	write(exportfd, num, l+1);
	close(exportfd);

	//printf("GPIO exported successfully\n");
	// Update the direction of the GPIO to be an output 
	sprintf(str, "/sys/class/gpio/gpio%s/direction", num);
	directionfd = open(str, O_RDWR);

	if (directionfd < 0) {
        printf("Cannot open GPIO direction it\n");
        exit(1);
	}

	l = strlen(dir);
	write(directionfd, dir, l+1);
	close(directionfd);

	//printf("GPIO direction set successfully\n");
	// Get the GPIO value ready to be toggled
	sprintf(str, "/sys/class/gpio/gpio%s/value", num);
	if (l == 2)
	    valuefd = open(str, O_RDONLY); // in
	else
    valuefd = open(str, O_WRONLY); // out
        if (valuefd < 0) {
	        printf("Cannot open GPIO value\n");
	        exit(1);
	    }
	return valuefd;
} 

void gpio_free(char *num) {
	int fd;
 	fd = open("/sys/class/gpio/unexport", O_WRONLY);
 	write(fd, num, strlen(num));
 	close(fd);
} 

void enable_freeze(void) {
    // example of equivalent operation done in the command line 
	// # echo 54 > /sys/class/gpio/export
	// # echo out > /sys/class/gpio/gpio54/direction
	// # echo 1 > /sys/class/gpio/gpio54/value

 	int out54; 
 	out54 = gpio_alloc((char*)"54", (char*)"out"); 
	write(out54,"1", 2); 
 	gpio_free((char*)"54");

}

void disable_freeze(void) {
    // example of equivalent operation done in the command line 
	// # echo 54 > /sys/class/gpio/export
	// # echo out > /sys/class/gpio/gpio54/direction
	// # echo 0 > /sys/class/gpio/gpio54/value

 	int out54; 
 	out54 = gpio_alloc((char*)"54", (char*)"out"); 
	write(out54,"0", 2); 
 	gpio_free((char*)"54");

}

void enable_pr(void) {
    // example of equivalent operation done in the command line 
    // # echo 1 > /sys/devices/amba.5/f8007000.ps7-dev-cfg/is_partial_bitstream

    int fd = open("/sys/devices/amba.5/f8007000.ps7-dev-cfg/is_partial_bitstream", O_WRONLY);
    if (fd == -1) {
        perror("Unable to open /sys/devices/amba.5/f8007000.ps7-dev-cfg/is_partial_bitstream");
        exit(1);
    }

    if (write(fd, "1", 1) != 1) {
        perror("Error writing to /sys/devices/amba.5/f8007000.ps7-dev-cfg/is_partial_bitstream");
        exit(1);
    }

    close(fd);
}

void disable_pr(void) {
    // example of equivalent operation done in the command line 
    // # echo 0 > /sys/devices/amba.5/f8007000.ps7-dev-cfg/is_partial_bitstream

    int fd = open("/sys/devices/amba.5/f8007000.ps7-dev-cfg/is_partial_bitstream", O_WRONLY);
    if (fd == -1) {
        perror("Unable to open /sys/devices/amba.5/f8007000.ps7-dev-cfg/is_partial_bitstream");
        exit(1);
    }

    if (write(fd, "0", 1) != 1) {
        perror("Error writing to /sys/devices/amba.5/f8007000.ps7-dev-cfg/is_partial_bitstream");
        exit(1);
    }

    close(fd);
}

int load_partial_bitstream(std::string fpga_path) {

        // wait for the streaming to finish
        std::this_thread::sleep_for(std::chrono::milliseconds(200));

        // Enable freeze and PR bitstream
        std::cout << "Enable freeze and PR bitstream" << std::endl;
        enable_pr();
        enable_freeze();

        // create the image loader arguments
        uhd::image_loader::image_loader_args_t image_loader_args;
        image_loader_args.args.set("type", "e3x0");
        image_loader_args.load_firmware = true; //(vm.count("no-fw") == 0);
        image_loader_args.load_fpga     = true; //(vm.count("no-fpga") == 0);
        image_loader_args.download      = false; // (vm.count("download") != 0);
        image_loader_args.firmware_path = "";
        image_loader_args.fpga_path     = fpga_path; //vm["fpga-path-pr"].as<std::string>();
        image_loader_args.out_path      = "";

        std::cout << "Loading the partial fpga bistream: "
                  << image_loader_args.fpga_path 
                  << std::endl;
                  
        //call image loader
        if (not uhd::image_loader::load(image_loader_args)) {
            std::cerr << "No applicable UHD devices found" << std::endl;
            return EXIT_FAILURE;
        }

        // Disable freeze and PR bitstream
        std::cout << "Disable freeze and PR bitstream" << std::endl;
        disable_pr();
        disable_freeze();

        std::this_thread::sleep_for(std::chrono::milliseconds(200));

        return EXIT_SUCCESS;

}

// end modifications SGA

int setup_and_stream(std::string file, std::string streamargs, 
    uhd::device3::sptr usrp, std::string block_id, uhd::rfnoc::block_id_t radio_ctrl_id, 
    size_t radio_chan, std::string block_args, size_t spp, std::string format,
    size_t total_num_samps, size_t spb, double rate, double total_time,
    bool bw_summary, bool stats, bool enable_size_map, bool continue_on_bad_packet) 
    { 


{ 
        /************************************************************************
         * Set up streaming
         ***********************************************************************/

        uhd::device_addr_t streamer_args(streamargs);

        uhd::rfnoc::graph::sptr rx_graph = usrp->create_graph("rfnoc_rx_to_file");
        usrp->clear();
        // Set the stream args on the radio:
        if (block_id.empty()) {
            // If no extra block is required, connect to the radio:
            streamer_args["block_id"]   = radio_ctrl_id.to_string();
            streamer_args["block_port"] = str(boost::format("%d") % radio_chan);
        } else {
            // Otherwise, see if the requested block exists and connect it to the radio:
            if (not usrp->has_block(block_id)) {
                std::cout << "Block does not exist on current device: " << block_id
                        << std::endl;
                return EXIT_FAILURE;
            }

            uhd::rfnoc::source_block_ctrl_base::sptr blk_ctrl =
                usrp->get_block_ctrl<uhd::rfnoc::source_block_ctrl_base>(block_id);

            if (not block_args.empty()) {
                // Set the block args on the other block:
                blk_ctrl->set_args(uhd::device_addr_t(block_args));
            }
            // Connect:
            std::cout << "Connecting " << radio_ctrl_id << " ==> " << blk_ctrl->get_block_id()
                    << std::endl;
            rx_graph->connect(
                radio_ctrl_id, radio_chan, blk_ctrl->get_block_id(), uhd::rfnoc::ANY_PORT);
            streamer_args["block_id"] = blk_ctrl->get_block_id().to_string();

            spp = blk_ctrl->get_args().cast<size_t>("spp", spp);
        }

        // create a receive streamer
        std::cout << "Samples per packet: " << spp << std::endl;
        uhd::stream_args_t stream_args(
            format, "sc16"); // We should read the wire format from the blocks
        stream_args.args        = streamer_args;
        stream_args.args["spp"] = boost::lexical_cast<std::string>(spp);
        std::cout << "Using streamer args: " << stream_args.args.to_string() << std::endl;
        uhd::rx_streamer::sptr rx_stream = usrp->get_rx_stream(stream_args);

        if (total_num_samps == 0) {
            std::signal(SIGINT, &sig_int_handler);
            std::cout << "Press Ctrl + C to stop streaming..." << std::endl;
        }

    #define recv_to_file_args() \
        (rx_stream,             \
            file,               \
            spb,                \
            rate,               \
            total_num_samps,    \
            total_time,         \
            bw_summary,         \
            stats,              \
            enable_size_map,    \
            continue_on_bad_packet)
        // recv to file
        if (format == "fc64")
            recv_to_file<std::complex<double>> recv_to_file_args();
        else if (format == "fc32")
            recv_to_file<std::complex<float>> recv_to_file_args();
        else if (format == "sc16")
            recv_to_file<std::complex<short>> recv_to_file_args();
        else
            throw std::runtime_error("Unknown data format: " + format);

        // finished
        std::cout << std::endl << "Done!" << std::endl << std::endl;

}
        return EXIT_SUCCESS;
    }

