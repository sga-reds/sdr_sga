##################################
##                              ##  
##	Tools installation      ##
##      UHD target              ##  
##################################

# source the UHD environment
source ~/rfnoc/installs/setup.env
uhd_usrp_probe --version
which uhd_usrp_probe

# Source the OE SDK environment setup file and test:
cd ~/rfnoc/oe
source ./environment-setup-armv7ahf-vfp-neon-oe-linux-gnueabi 
echo $CC

# Cross-Compiling UHD
cd ~/rfnoc/src/uhd/host
mkdir build-arm 
cd build-arm
cmake -DCMAKE_TOOLCHAIN_FILE=../host/cmake/Toolchains/oe-sdk_cross.cmake -DCMAKE_INSTALL_PREFIX=/usr -DENABLE_E300=ON -DENABLE_GPSD=ON -DENABLE_RFNOC=ON ../
make -j4
make install DESTDIR=~/rfnoc/e300
make install DESTDIR=~/rfnoc/oe/sysroots/armv7ahf-vfp-neon-oe-linux-gnueabi/

# Create an Environment Setup File
cd ~/rfnoc/e300
echo "LOCALPREFIX=~/newinstall/usr" >  setup.env
echo "export PATH=\$LOCALPREFIX/bin:\$PATH" >>  setup.env
echo "export LD_LOAD_LIBRARY=\$LOCALPREFIX/lib:\$LD_LOAD_LIBRARY" >>  setup.env
echo "export LD_LIBRARY_PATH=\$LOCALPREFIX/lib:\$LD_LIBRARY_PATH" >>  setup.env
echo "export PYTHONPATH=\$LOCALPREFIX/lib/python2.7/site-packages:\$PYTHONPATH" >>  setup.env
echo "export PKG_CONFIG_PATH=\$LOCALPREFIX/lib/pkgconfig:\$PKG_CONFIG_PATH" >>  setup.env
echo "export GRC_BLOCKS_PATH=\$LOCALPREFIX/share/gnuradio/grc/blocks:\$GRC_BLOCKS_PATH" >>  setup.env
echo "export UHD_RFNOC_DIR=\$LOCALPREFIX/share/uhd/rfnoc/" >>  setup.env
echo "export UHD_IMAGES_DIR=\$LOCALPREFIX/share/uhd/images" >>  setup.env
echo "" >>  setup.env

# Copy default FPGA images
# form images location: ~/rfnoc/installs/share/uhd/images
mkdir -p ~/rfnoc/e300/usr/share/uhd/images
cd ~/rfnoc/e300/usr/share/uhd/images
cp -Rv ~/rfnoc/installs/share/uhd/images/usrp_e310_fpga* .
cp -Rv ~/rfnoc/installs/share/uhd/images/usrp_e3xx_fpga_idle* .

# cp -Rv ~/rfnoc/src/fpga/e310_fpga_images/* .

# login in the card, mount the new_install folder, source the environment file, test
# sudo picocom -b 115200 /dev/ttyUSB0
# TARGET_IP="192.168.10.2"
# ssh root@$TARGET_IP
# HOST_IP="192.168.10.1"
# mkdir -p ~/newinstall
# sshfs saragrassi@$HOST_IP:/home/saragrassi/rfnoc/e300 newinstall/
# source ~/newinstall/setup.env 
# which uhd_usrp_probe
# uhd_usrp_probe --version

# cd ~/newinstall/usr/lib/uhd/examples/
# ./rx_samples_to_file --freq 100e6 --gain 0 --ant TX/RX --rate 1e6 --duration 10 --null


