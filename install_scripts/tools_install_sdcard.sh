
##################################
##                              ##  
##	Tools installation      ##
##      sd-card + SDK           ##  
##################################


# set the user, e.g 
# USER="saragrassi"
USER=

## Prepare the sd-card

## insert and identify the sd-card, e.g.
lsblk
# NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
# sda           8:0    1  14.9G  0 disk 
# └─sda1        8:1    1  14.9G  0 part 


# set the sdcard, e.g. 
# SD_CARD="sda"
SD_CARD=

# umount the card if necessary, e.g. 
sudo umount /media/$USER/*

# Create a directory and get the SD card release-4 image into it. Uncompress the image and write
cd ~
mkdir -p ~/rfnoc/sdcard
cd ~/rfnoc/sdcard/
wget https://files.ettus.com/e3xx_images/e3xx-release-4/ettus-e3xx-sg3/sdimage-gnuradio-dev.direct.xz
xzdec sdimage-gnuradio-dev.direct.xz > sdimage-gnuradio-dev.direct
sudo dd if=sdimage-gnuradio-dev.direct of=/dev/$SD_CARD bs=1M status=progress
sync

# insert the card in the E310 and log in with com terminal:
# sudo picocom -b 115200 /dev/ttyUSB0

# connect and configure the ethernet connection:
# ifconfig
# ifconfig eth0 down
# ifconfig eth0 192.168.10.2 netmask 255.255.255.0 up
# ifconfig

# test some commands in the card:
# which uhd_usrp_probe
# uhd_usrp_probe --version
# uhd_usrp_probe
# -- Loading FPGA image: /usr/share/uhd/images/usrp_e310_fpga_sg3.bit... done
# which gnuradio-config-info
# gnuradio-config-info --version



