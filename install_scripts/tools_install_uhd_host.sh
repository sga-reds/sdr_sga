##################################
##                              ##  
##	Tools installation      ##
##                              ##  
##################################

# step-by-step process involved in building and installing UHD and GNU Radio to a custom prefix

# Update your OS
sudo apt update
sudo apt upgrade -y

# Installing Dependencies
# see https://kb.ettus.com/Building_and_Installing_UHD_and_GNU_Radio_to_a_Custom_Prefix
# see https://kb.ettus.com/Software_Development_on_the_E3xx_USRP_-_Building_RFNoC_UHD_/_GNU_Radio_/_gr-ettus_from_Source
sudo apt -y install git swig cmake doxygen build-essential libboost-all-dev libtool libusb-1.0-0 libusb-1.0-0-dev libudev-dev libncurses5-dev libfftw3-bin libfftw3-dev libfftw3-doc libcppunit-1.14-0 libcppunit-dev libcppunit-doc ncurses-bin cpufrequtils python-numpy python-numpy-doc python-numpy-dbg python-scipy python-docutils qt4-bin-dbg qt4-default qt4-doc libqt4-dev libqt4-dev-bin python-qt4 python-qt4-dbg python-qt4-dev python-qt4-doc python-qt4-doc libqwt6abi1 libfftw3-bin libfftw3-dev libfftw3-doc ncurses-bin libncurses5 libncurses5-dev libncurses5-dbg libfontconfig1-dev libxrender-dev libpulse-dev swig g++ automake autoconf libtool python-dev libfftw3-dev libcppunit-dev libboost-all-dev libusb-dev libusb-1.0-0-dev fort77 libsdl1.2-dev python-wxgtk3.0 git libqt4-dev python-numpy ccache python-opengl libgsl-dev python-cheetah python-mako python-lxml doxygen qt4-default qt4-dev-tools libusb-1.0-0-dev libqwtplot3d-qt5-dev pyqt4-dev-tools python-qwt5-qt4 cmake git wget libxi-dev gtk2-engines-pixbuf r-base-dev python-tk liborc-0.4-0 liborc-0.4-dev libasound2-dev python-gtk2 libzmq3-dev libzmq5 python-requests python-sphinx libcomedi-dev python-zmq libqwt-dev libqwt6abi1 python-six libgps-dev libgps23 gpsd gpsd-clients python-gps python-setuptools python3-pyqt5 dnsmasq sshfs

# Create the Working and Install Directories
mkdir -p ~/rfnoc/src
mkdir -p ~/rfnoc/installs

# clone the directory fpga
# cd ~/rfnoc/src    
# git clone https://github.com/EttusResearch/fpga
# cd fpga/
# git checkout v3.14.1.1

# Building UHD
cd ~/rfnoc/src
git clone --recursive https://github.com/EttusResearch/uhd
cd uhd
git checkout v3.14.1.1
git submodule update --init --recursive
cd host
mkdir build-host
cd build-host
cmake -DCMAKE_INSTALL_PREFIX=~/rfnoc/installs -DENABLE_E300=ON -DENABLE_GPSD=ON -DENABLE_RFNOC=ON ../
make -j4
make install

# Configuring Thread Priority
# sudo groupadd usrp
# sudo usermod -aG usrp $USER
# # Append the following line to end of /etc/security/limits.conf file:
# sudo sh -c "echo '@usrp\t-\trtprio\t99' >> /etc/security/limits.conf"
# You must log out and log back in for this setting to take effect.

# create the environment file
cd ~/rfnoc/installs
echo "LOCALPREFIX=~/rfnoc/installs" >  setup.env
echo "export PATH=\$LOCALPREFIX/bin:\$PATH" >>  setup.env
echo "export LD_LOAD_LIBRARY=\$LOCALPREFIX/lib:\$LD_LOAD_LIBRARY" >>  setup.env
echo "export LD_LIBRARY_PATH=\$LOCALPREFIX/lib:\$LD_LIBRARY_PATH" >>  setup.env
echo "export PYTHONPATH=\$LOCALPREFIX/lib/python2.7/site-packages:\$PYTHONPATH" >>  setup.env
echo "export PYTHONPATH=\$LOCALPREFIX/lib/python2.7/dist-packages:\$PYTHONPATH" >>  setup.env
echo "export PKG_CONFIG_PATH=\$LOCALPREFIX/lib/pkgconfig:\$PKG_CONFIG_PATH" >>  setup.env
echo "export UHD_RFNOC_DIR=\$LOCALPREFIX/share/uhd/rfnoc/" >>  setup.env
echo "export UHD_IMAGES_DIR=\$LOCALPREFIX/share/uhd/images" >>  setup.env

# source the environment file
source ~/rfnoc/installs/setup.env

# Verify UHD Installation
uhd_usrp_probe --version
which uhd_usrp_probe

# Download UHD FPGA Images
uhd_images_downloader --version
uhd_images_downloader -t e310
# Images destination: ~/rfnoc/installs/share/uhd/images

# [INFO] No inventory file found at /home/saragrassi/rfnoc/installs/share/uhd/images/inventory.json. Creating an empty one.



