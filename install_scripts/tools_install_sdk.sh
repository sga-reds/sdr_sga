##################################
##                              ##  
##	Tools installation      ##
##      SDK                     ##  
##################################

## SDK Setup

# Download the OE SDK for the E310 release-4 into the ~/rfnoc/src/ directory:
cd ~
mkdir -p ~/rfnoc/src
cd ~/rfnoc/src
wget http://files.ettus.com/e3xx_images/e3xx-release-4/oecore-x86_64-armv7ahf-vfp-neon-toolchain-nodistro.0.sh

# Install the SDK to the ~/rfnoc/oe directory:
mkdir -p ~/rfnoc/oe
bash oecore-x86_64-armv7ahf-vfp-neon-toolchain-nodistro.0.sh -d ~/rfnoc/oe -y
ls ~/rfnoc/oe
 
# Source the OE SDK environment setup file and test:
cd ~/rfnoc/oe
source ./environment-setup-armv7ahf-vfp-neon-oe-linux-gnueabi 
echo $CC

